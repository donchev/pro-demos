<html>
    <head>
        <title><?php echo $page // well we print the $data['page'] that has value "Вход" ?></title>
    </head>
    <body>
        <?php echo isset($error) && $error ? $error : ''; // our custom error message for the wrong password comes here ?>
        <?php echo validation_errors(); // print validation errors (The Username field is required... etc.) ?>
        <form id="form1" name="form1" method="post" action="<?php echo base_url(); ?>index.php/home/do_login">
            <fieldset>
                <div class="form-group">
                    <input placeholder="Име" name="username" type="text" autofocus>
                </div>
                <div class="form-group">
                    <input placeholder="Парола" name="password" type="password" value="">
                </div>
                <input type="submit" value="Login">
            </fieldset>
        </form>
    </body>
</html>