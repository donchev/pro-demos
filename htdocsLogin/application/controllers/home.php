<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('form_validation'); // load form validation library for the whole class
    }

	public function index()
	{
		if ($this->session->userdata('is_logged_in')) { // check if there's an active user session
            $this->load->view('vwHome');
        } else { // if there's no active session redirect user to login page
            $data['page'] = "Вход";
            $this->load->view('vwLogin', $data);
        }	
	}

	public function do_login()
	{

		// $salt = 'asdllvkcxo-094329xnz249389U@IO#U()@$*'; 
        // echo md5($salt . "teampro");
		// We use this code if we want to generate new password. Okay?

		$data['page'] = "Вход";

		if ($this->session->userdata('is_logged_in')) { // check if there's in an active session
            redirect('home');
        } else {

            $user = $this->input->post('username'); // getting the information from the form
            $password = $this->input->post('password'); // still getting the information but the password field

            $this->form_validation->set_rules('username', 'Username', 'required'); // we want to submit a form only is the users have writter something, right?
            $this->form_validation->set_rules('password', 'Password', 'required'); // we still want the same as the line before this

            if ($this->form_validation->run() == FALSE) { // form_valition() library from CodeIgniter - you can find more information about it from their website
                $this->load->view('vwLogin', $data); // If the form is not successful we redirect the user again to it
            } else {
                $salt = 'asdllvkcxo-094329xnz249389U@IO#U()@$*'; // a random string that will be incloded in the password encryption (not the best practice but it's good for now)
                $enc_pass = md5($salt . $password); // combining our salt with md5 standart encryption, you can google md5 to see that it's more then weak if it stands alone

                $sql = "SELECT * FROM `users` WHERE `username` = ? AND `password` = ?"; // a query to check if the user and pass match
                $val = $this->db->query($sql, array($user, $enc_pass)); // sending the query

                if ($val->num_rows() > 0) { // well, here we check if there's more then 1 results, right? If the user and pass are currect we will recieve more then zero results. You can check num_rows() from CodeIgniter library
                    foreach ($val->result_array() as $recs => $res) { // a foreach loop with result_array() still from CodeIgniter
                        $this->session->set_userdata(array( // set_userdata - now we are talking about sessions and session data.
                            'id' => $res['id'], // we create a session data with ID
                            'username' => $res['username'], // with the user's username
                            'is_logged_in' => true // and that he's logged in, obviously 
                                )
                        );
                    }
                    redirect('home'); // and finally we redirect the user to the home page
                } else {
                    $err['error'] = 'Wrong username or password :('; // well, sometimes we have to do bad things like this 
                    $this->load->view('vwLogin', $err); // and we have to tell the user the bad news
                }
            }
        }
	}

	public function logout() {
        $this->session->unset_userdata('id'); // we unset/delete the ID information from the session data
        $this->session->unset_userdata('username'); // we do the same with the username
        $this->session->unset_userdata('is_logged_in'); // and also with the 'is_logged_in' state
        $this->session->sess_destroy(); // and we DESTROY IT !
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0"); // this is useful if we use cookies but they are bad!
        $this->output->set_header("Pragma: no-cache");
        redirect('home', 'refresh'); // and finally we refresh the whole page. Any questions?
    }

}
