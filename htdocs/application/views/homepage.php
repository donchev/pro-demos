<html>
	<head>
		<title>Workshop site</title>
		<link rel="stylesheet" href="<?php echo base_url() ?>/assets/style.css">
	</head>
	<body>
		<div id="main-content">
			<div id="posts">

				<?php foreach($events as $event): ?>

					<div class="post-wrapper">
					<div class="post-data">
						<div class="author">
							<div class="author-image">
								<img src="<?php echo base_url() ?>assets/images/image1.jpg" class="image" alt="">
							</div>
							
							<div class="author-action">
								<p class="name">
									<a href=""><?php echo $event["personName"] ?></a> 
									<small>is going to an event</small>
								</p>
								<p class="time">
									13 minutes ago
								</p>
							</div>
						</div>

						<div class="event">
							<img src="<?php echo base_url() ?>assets/images/image2.jpg" class="image" alt="">

							<div class="event-description">
								<div class="wrapper">
									<div class="title">
										<a href=""><?php echo $event["eventName"] ?></a>
									</div>

									<div class="date-time">
										<p><?php echo $event["eventDate"] ?></p>
									</div>

									<div class="location">
										<p>Sample location in Sample city, country</p>
									</div>

									<div class="attendants">
										<a href="">15 are going</a>
									</div>
								</div>

								<div class="join">
									<a href="">Join</a>
								</div>
							</div>
						</div>

						<div class="user-actions">
							<div class="like">
								<a href="">Like</a>
							</div>

							<div class="comment">
								<a href="">Comment</a>
							</div>

							<div class="share">
								<a href="">Share</a>
							</div>
						</div>
					</div>
				</div>

				<?php endforeach; ?>


				<br>
				<br>
				<br>

			</div>
		</div>
	</body>
</html>