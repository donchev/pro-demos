<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->database();
		
		$query = $this->db->query("SELECT * FROM `events`");
		$data["events"] = $query->result_array();

		$this->load->view('homepage', $data);
	}

	public function hello()
	{
		echo "Hello World";
	}

}
